package ru.app.relationships.controllers;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.managment.Command;
import com.managment.FileSystem;
import com.managment.PhoneRequest;
import com.managment.StateCommand;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.app.relationships.clients.GrpcClientCommandCenter;
import ru.app.relationships.service_grpc_client.ServiceCommandCenter;

@RestController
public class ConnectionController {
    private final ServiceCommandCenter serviceCommandCenter;

    public ConnectionController(ServiceCommandCenter serviceCommandCenter) {
        this.serviceCommandCenter = serviceCommandCenter;

    }

    @GetMapping("/test")
    public ResponseEntity<String> test(@RequestParam("path") String path) throws InvalidProtocolBufferException {


        var string = JsonFormat.printer().print(serviceCommandCenter.sendCommand(PhoneRequest.newBuilder()
                .setCommand(Command.GET_FILES_LIST)
                        .setState(StateCommand.WAITING)
                        .setFileSystem(FileSystem.newBuilder()
                                .setTransitionPath(path).build())
                .setPhoneNumber("+79266955807").build()));

        return ResponseEntity.ok(string);
    }
}
