package ru.app.relationships.clients;

import com.managment.CommandCenterGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class GrpcClientCommandCenter {
    private final CommandCenterGrpc.CommandCenterBlockingStub blockingStub;

    public GrpcClientCommandCenter() {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:5555")
                .usePlaintext().build();
        blockingStub = CommandCenterGrpc.newBlockingStub(channel);
    }

    public CommandCenterGrpc.CommandCenterBlockingStub getBlockingStub(){
        return blockingStub;
    }
}
