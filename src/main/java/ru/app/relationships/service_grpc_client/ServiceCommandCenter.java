package ru.app.relationships.service_grpc_client;

import com.managment.PhoneRequest;
import com.managment.PhoneResponse;
import org.springframework.stereotype.Service;
import ru.app.relationships.clients.GrpcClientCommandCenter;

@Service
public class ServiceCommandCenter {
    public final GrpcClientCommandCenter grpcClientCommandCenter;

    public ServiceCommandCenter(GrpcClientCommandCenter grpcClientCommandCenter) {
        this.grpcClientCommandCenter = grpcClientCommandCenter;
    }

    public PhoneResponse sendCommand(PhoneRequest phoneRequest){
        return grpcClientCommandCenter.getBlockingStub().setProcessingCommand(phoneRequest);
    }
}
